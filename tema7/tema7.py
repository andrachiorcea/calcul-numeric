import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
import random
from tkinter import *
from tkinter import scrolledtext, Text

def calcul_lagrange_noduri_existente(xp, x, y):
    rez = y[0]
    for i in range(1, len(x)):
        rez += calcul_diferente_divizate(i, x, y) * termen_produs(xp, i, x)
    return rez


def calcul_lagrange_noduri_exsitente_aitken(xp, x, y):
    aitk = calcul_diferente_divizate_aitken(x, y)
    rez = y[0]
    for i in range(1, len(x)):
        rez += aitk[i] * termen_produs(xp, i, x)
    return rez


def termen_produs(xp, k, x):
    produs = 1
    for i in range(0, k):
        produs *= xp - x[i]
    return produs


def calcul_diferente_divizate(k, x, y):
    rez = 0
    for i in range(0, k+1):
        produs = 1
        for j in range(0, k+1):
            if j != i:
                produs *= x[i] - x[j]
        rez += y[i]/produs
    return rez


def calcul_diferente_divizate_aitken(x, y):
    rez = [[] for i in range(0, len(y))]
    for i in range(0, len(rez)):
        for j in range(0, i+1):
            rez[i].append(0)
    for i in range(0, len(y)):
        rez[i][0] = y[i]
        if i > 0:
            rez[i][1] = (y[i] - y[i-1])/(x[i]-x[i-1])
    for i in range(2, len(y)):
        for j in range(2, i+1):
            rez[i][j] = (rez[i][j-1] - rez[i-1][j-1])/(x[i]-x[i-j])
    to_ret = []
    for i in range(0, len(y)):
        to_ret.append(rez[i][i])
    return to_ret


def cautare_interval(xp, x):
    for i in range(0, len(x)-1):
        if x[i] <= xp <= x[i+1]:
            return i


def calcul_lagrange_numpy(inp, x, y):
    xn = np.array(x)
    yn = np.array(y)
    f = interpolate.interp1d(xn, yn)
    return f(inp)


def calcul_modul(rez, valf):
    rez = abs(rez - valf)
    return rez


def calcul_vector_h(x):
    h = [0 for i in range(0, len(x)-1)]
    for i in range(0, len(x)-1):
        h[i] = x[i+1] - x[i]
    return h


def calcul_matrice_H(h):
    H = np.zeros((len(h), len(h)))
    for i in range(0, len(h)):
        if i == 0:
            H[i][i] = 2*h[0]
            H[i+1][i] = h[0]
        elif i == len(h)-1:
            H[i][i] = 2*h[-1]
            H[i-1][i] = h[-1]
        else:
            H[i][i] = 2*(h[i-1]+h[i])
            H[i-1][i] = h[i-1]
            H[i+1][i] = h[i]
    return H


def calcul_vector_f(h, da, db, x, y):
    f = np.zeros(len(x)-1)
    f[0] = 6*((y[1]-y[0])/h[0] - da)
    for i in range(1, len(x)-2):
        f[i] = 6*((y[i+1]-y[i])/h[i] - (y[i]-y[i-1])/h[i-1])
    f[-1] = 6*(db - (y[-1] - y[-2])/h[-1])
    return f


def calcul_A(da, db, x, y):
    h = calcul_vector_h(x)
    H = calcul_matrice_H(h)
    f = calcul_vector_f(h, da, db, x, y)
    A = np.linalg.solve(H, f)
    return A, h


def calcul_solutie_cubic_spline(da, db, nr, x, y):
    i_start = cautare_interval(nr, x)
    i_end = i_start + 1
    A, h = calcul_A(da, db, x, y)
    t1 = (((nr - x[i_start])**3)*A[i_end])/6*h[i_start]
    t2 = (((x[i_end]-nr)**3)*A[i_start])/6*h[i_start]
    t3 = (((y[i_end]-y[i_start])/h[i_start]) - (h[i_start]*(A[i_end] - A[i_start]))/6)*nr
    t4 = ((x[i_end]*y[i_start]-x[i_start]*y[i_end])/h[i_start]) - (h[i_start]*(x[i_end]*A[i_start]-x[i_start]*A[i_end]))/6
    sol = t1 + t2 + t3 + t4
    return sol


def grafic_functia_f_ex1():
    plt.ylim([-600, 100])
    plt.xlim([0, 5])
    vec = [0, 1, 1.5, 2, 3, 4, 5]
    val = [50, 47, 30.3125, -2, -121, -310, -545]
    plt.plot(vec, val)
    plt.show()


def grafic_functie_f_ex2():
    plt.ylim([0, 200])
    plt.xlim([0, 7])
    vec = [0, 1, 1.5, 2, 3, 4, 5]
    val = [50, 47, 30.3125, -2, -121, -310, -545]
    plt.plot(vec, val)
    plt.show()


def ex1_lagrange_aitken():
    x = [0, 1, 2, 3, 4, 5]
    y = [50, 47, -2, -121, -310, -545]
    rez = calcul_lagrange_noduri_exsitente_aitken(1.5, x, y)
    return rez


def ex1_cubic_spline():
    grafic_functia_f_ex1()
    x = [0, 1, 2, 3, 4, 5]
    y = [50, 47, -2, -121, -310, -545]
    sol = calcul_solutie_cubic_spline(6, -244, 1.5, x, y)
    return sol


def ex1_cubic_spline_din_grafic():
    x = np.array([0, 1, 2, 3, 4, 5])
    y = np.array([50, 47, -2, -121, -310, -545])
    cs = CubicSpline(x, y, bc_type='natural')
    return cs(1.5)


def ex2():
    da = 4
    db = 100
    epsilon = 10 ** (-6)
    x = [i for i in range(1, 6)]
    y = [i for i in range(1, 6)]
    for i in range(0, 5):
        y[i] = x[i]**3 + 3*(x[i]**2) - 5*x[i] + 12
    xp = 1
    while xp in x:
        xp = random.uniform(1, 5)
    valoare_in_punct = xp**3 + 3*(xp**2) - 5*xp + 12
    print(valoare_in_punct)
    rez_lagrange_aitken = calcul_lagrange_noduri_existente(xp, x, y)
    print(rez_lagrange_aitken)
    rez_cubic_spline = calcul_solutie_cubic_spline(da, db, xp, x, y)
    print(rez_cubic_spline)
    modull = calcul_modul(rez_lagrange_aitken, valoare_in_punct)
    moduls = calcul_modul(rez_cubic_spline, valoare_in_punct)
    if modull < epsilon:
        val = "True"
    else:
        val = "False"
    plt.ylim([0, 200])
    plt.xlim([1, 7])
    plt.plot(x, y)
    plt.show()
    return rez_lagrange_aitken, rez_cubic_spline, valoare_in_punct, modull


def solver():
    global ex1l, ex1cs, ex2l, ex2cs, modul, xp
    ex1l = ex1_lagrange_aitken()
    ex1cs = ex1_cubic_spline()
    ex2l, ex2cs, xp, modul = ex2()


def custom_print(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(to_print)))
    return


class Application(Frame):
    def createWidgets(self):
        self.a2 = Button(self , command = lambda : custom_print(ex1l))
        self.a2["text"] = "Rezultat Lagrange ex1"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=1, column=0, sticky='nesw')

        self.a4 = Button(self , command = lambda : custom_print(ex1cs))
        self.a4["text"] = "Rezultat Cubic spline ex1"
        self.a4.configure(bg="salmon1")
        self.a4.grid(row=2, column=0, sticky='nesw')

        self.a3 = Button(self , command = lambda : custom_print(xp))
        self.a3["text"] = "Valoare exacta ex2"
        self.a3.configure(bg="light coral")
        self.a3.grid(row=3, column=0, sticky='nesw')

        self.a3 = Button(self , command = lambda : custom_print(ex2l))
        self.a3["text"] = "Rezultat Lagrange ex2"
        self.a3.configure(bg="light coral")
        self.a3.grid(row=4, column=0, sticky='nesw')

        self.a5 = Button(self , command = lambda : custom_print(ex2cs))
        self.a5["text"] = "Rezultat Cubic spline ex2"
        self.a5.configure(bg="light coral")
        self.a5.grid(row=5, column=0, sticky='nesw')

        self.a6 = Button(self , command = lambda : custom_print(modul))
        self.a6["text"] = "Rezultat Cubic spline ex2"
        self.a6.configure(bg="aquamarine")
        self.a6.grid(row=6, column=0, sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=16, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()


solver()
root = Tk()
app = Application(master=root)
app.mainloop()
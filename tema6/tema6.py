from tkinter import *
from tkinter import scrolledtext, Text
import numpy as np
# coef = np.array([1.0, -6.0, 11.0, -6.0])
# puteri = np.array([3, 2, 1, 0])

def get_interval(coef):
    absolute_coef = np.absolute(coef)
    max_coef = np.max(absolute_coef)
    a0 = coef[0]
    r = (a0 + max_coef)/a0
    return -r, r


def calculeaza_derivata(coef):
    coefc = coef.copy()
    length=len(coefc)
    for i in range(0, length-1):
        coefc[i] = coefc[i]*(len(coefc)-1-i)
    coefc = coefc[:-1]
    return coefc


def evaluare_polinom(coef, putere, x):
    rez = 0
    for i in range(0, len(coef)):
        rez += coef[i]*(x**putere[i])
    return rez


def calculeaza_A(coef, putere, x):
    prima_derivata = calculeaza_derivata(coef, putere)
    doua_derivata = calculeaza_derivata(prima_derivata[0], prima_derivata[1])
    calcul_x = horner(coef, x)
    calcul_xd = horner(prima_derivata[0], x)
    calcul_xdd = horner(doua_derivata[0], x)
    A = 2*calcul_xd*calcul_xd - calcul_x*calcul_xdd
    return A

def gcd(a,b):
    f1=a.copy()
    f2=b.copy()
    while len(f2)>1:
        q,f3=np.polydiv(np.array(f1),np.array(f2))
        f1=f2.copy()
        f2=f3.tolist()
    return f1

def halley_method(coef, x):
    prima_derivata = calculeaza_derivata(coef)
    if not np.array_equal(np.array(coef),np.array(gcd(coef,prima_derivata))):
        q,r=np.polydiv(np.array(coef),gcd(coef,prima_derivata))
        coef=np.polyadd(q,r).tolist()
    k = 1
    delta = 0
    epsilon = 1/10**8
    kmax = 1000
    while True:
        prima_derivata = calculeaza_derivata(coef)
        doua_derivata = calculeaza_derivata(prima_derivata)
        calcul_x = horner(coef, x)
        calcul_xd = horner(prima_derivata, x)
        calcul_xdd = horner(doua_derivata, x)
        A = 2 * calcul_xd * calcul_xd - calcul_x * calcul_xdd
        if abs(A) < epsilon:
            break
        delta = (calcul_x*calcul_xd)/A
        x = x - delta
        k += 1
        if abs(delta) < epsilon or k > kmax or abs(delta) > 10**8:
            break
    if abs(delta)<epsilon:
        f = open("rez_halley.txt", "r+")
        contents = []
        v = f.readline()
        while v:
            valoare = float(v.strip())
            contents.append(valoare)
            v = f.readline()
        if x not in contents:
            f.write(str(x) + "\n")
        return x
    else:
        print("Divergenta")


def horner(coef,x):
    if len(coef)<1:
        return 0
    b=coef[0]
    for i in range(1, len(coef)):
        b=b*x+coef[i]
    return b

# print(halley_method(coef))
# print(halley_method([1, -6, 13, -12, 4]))

#print(gcd([1,-6,13,-12,4],[4,-18,26,-12]))
#print(gcd([1,0,1,1,0,1],[1,0,1,0]))

def parse_coef(inp):
    for i  in range(0,len(inp)):
        inp[i] = float(inp[i])
    return inp

def custom_print():
    global coef
    coef = app.a1.get("1.0", END).strip().split(", ")
    x = app.a3.get("1.0", END).strip()
    x = float(x[0])
    c = parse_coef(coef)
    rez = halley_method(c, x)
    print(rez)
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(rez)))
    return

def custom_print1(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(to_print)))
    return

class Application(Frame):
    def createWidgets(self):
        self.a1 = Text(self, height=1, width=1)
        self.a1.grid(row=0, column=0, sticky='nesw')

        self.a3 = Text(self, height=1, width=1)
        self.a3.grid(row=1, column=0, sticky='nesw')

        self.a2 = Button(self , command = lambda : custom_print())
        self.a2["text"] = "Rezultat metoda Halley"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=2, column=0, sticky='nesw')

        self.a4 = Button(self , command = lambda : custom_print1(get_interval(coef)))
        self.a4["text"] = "Interval Solutie"
        self.a4.configure(bg="salmon1")
        self.a4.grid(row=3, column=0, sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=16, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
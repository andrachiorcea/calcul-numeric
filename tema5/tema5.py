import random
import numpy as np
from tkinter import *
from tkinter import scrolledtext


def generare_matrice(n):
    matrice = [[] for i in range(0, n)]
    # generare elemente nenule digaonala principala
    for i in range(0,n):
        valoare = 0
        while valoare == 0:
            valoare = random.uniform(0, 100)
        matrice[i].append((valoare, i))
    nr_elemente_nenule = n
    while nr_elemente_nenule < int(n*n/4):
        valoare = 0
        coloana = 0
        linie = 0
        val_exists = True
        # verificare ca nu suntem pe diag. principala si ca nu avem val pe poz respectiva
        while (coloana == linie) or val_exists or (coloana <= linie):
            coloana = random.randint(0, n-1)
            linie = random.randint(0, n-1)
            i = 0
            nr_el_linie = len(matrice[linie])
            while i < nr_el_linie:
                if matrice[linie][i][1] == coloana:
                    val_exists = True
                    break
                val_exists = False
                i += 1
        # verificare ca valoarea nu este nenula
        while valoare == 0:
            valoare = random.uniform(0, 100)
        matrice[linie].append((valoare, coloana))
        matrice[coloana].append((valoare, linie))
        nr_elemente_nenule += 1
    return matrice


def generare_matrice_fisier(fisier):
    f = open(fisier)
    n = int(f.readline())
    matrice = [[] for i in range(0, n)]
    v = f.readline()
    v = f.readline()
    while v:
        l = v.split(", ")
        valoare = float(l[0])
        linie = int(l[1])
        coloana = int(l[2])
        matrice[linie].append((round(valoare, 9), coloana))
        v = f.readline()
    f.close()
    return matrice


def get_element_at_position(matrice, i, j):
    try:
        for el in matrice[i]:
            if el[1] == j:
                return el[0]
    except:
        return 0


def position_of_indices(m, i, j):
    for index in range(0,len(m[i])):
        if m[i][index][1] == j:
             return index
    return None


def verificare_simetrie(matrice):
    n = len(matrice)
    for i in range(0,n-1):
        for j in range(i+1, n):
            el1 = get_element_at_position(matrice, i, j)
            el2 = get_element_at_position(matrice, j, i)
            if el1 != el2 and (el1 != 0 and el2 != 0):
                return False
    return True


def multiply_matrix(m1, m2):
    mMult = [0]*len(m1)
    for i in range(len(m1)):
        for k in range(len(m2)):
            m1Pos = get_element_at_position(m1, i, k)
            if m1Pos is not None:
                mMult[i] += m1Pos * m2[k]
    return mMult


def add_matrix(m, v):
    mPlus=[[] for i in range(0, len(m))]
    for i in range(0,len(m)):
        for j in range(0,len(m)):
            x=get_element_at_position(m, i, j)
            mPlus[i].append((x+v[j], j))
    return mPlus


def generare_vector_random(n):
    x = np.random.rand(n)
    vect = x/np.linalg.norm(x)
    return vect.tolist()


def metoda_puterii(matrice, n):
    v = generare_vector_random(n)
    w = multiply_matrix(matrice, v)
    l = np.dot(np.array(w), np.array(v))
    k = 0
    kmax = 1000000
    epsilon = 1/10**9
    while True:
        v = w/np.linalg.norm(w)
        w = multiply_matrix(matrice, v)
        l = np.dot(np.array(w), np.array(v))
        k += 1
        to_check = np.linalg.norm([w[y]-v[y]*l for y in range(0, len(v))])
        if (to_check <= n*epsilon) or (k > kmax):
            break
    if k > kmax:
        return "Nimic"
    else:
        return l, v


def write_result_to_file(fisier_in, fisier_out, size):
    f = open(fisier_out, "w")
    rezultat = metoda_puterii(generare_matrice_fisier(fisier_in), size)
    f.write(str(rezultat[0])+"\n")
    f.write(str(rezultat[1]))
    f.close()
    print("Done")


def get_result_from_file(fisier):
    f = open(fisier, "r")
    valoare_proprie = f.readline().replace('\n', '')
    vector_asociat = f.read().replace('\n', '')
    return valoare_proprie, vector_asociat


def custom_print(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(to_print)))
    return


def solver():
    global matrice_random, rez_svd
    global valori_proprii, vector_asociat
    matrice_random = generare_matrice(500)
    print(matrice_random)
    rezultat_matrice_random = metoda_puterii(matrice_random, 500)
    fisiere = ["rez_sim_500.txt", "rez_sim_1000.txt", "rez_sim_1500.txt"]
    matrix = [[2, 4], [1, 3], [0, 0], [0, 0]]
    b = [6, 2, 3 , 4]
    valori_proprii = []
    vector_asociat = []
    valori_proprii.append(rezultat_matrice_random[0])
    vector_asociat.append(rezultat_matrice_random[1])
    for file in fisiere:
        rez = get_result_from_file(file)
        valori_proprii.append(rez[0])
        vector_asociat.append(rez[1])
    rez_svd = SVD(matrix, b)


def SVD(m, b):
    rank = np.linalg.matrix_rank(m)
    cond_factor = np.linalg.cond(m)
    u, singular_values, vt = np.linalg.svd(np.array(m))
    u = u.tolist()
    singular_values = singular_values.tolist()
    vt = vt.tolist()
    v = np.transpose(np.array(vt))
    ut = np.transpose(np.array(u))

    s_inv = [[0 for i in range(0, len(m))] for i in range(0, len(m[0]))]
    for i in range(0, len(singular_values)):
        s_inv[i][i] = 1/singular_values[i]

    m_inv = np.dot(np.array(v), np.array(s_inv))
    m_inv = np.dot(np.array(m_inv), np.array(ut)).tolist()

    x = np.dot(np.array(m_inv), np.array(b)).tolist()
    ax = np.dot(np.array(m), np.array(x))
    norm = np.linalg.norm(np.array([b[i]-ax[i] for i in range(0, len(b))]))
    return rank, cond_factor, u, singular_values, vt, m_inv, x, norm


class Application(Frame):
    def createWidgets(self):
        self.a2 = Button(self , command = lambda : custom_print(valori_proprii[0]))
        self.a2["text"] = "Valoare proprie matrice random"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=0, column=0, sticky='nesw')

        self.a3 = Button(self, command = lambda : custom_print(vector_asociat[0]))
        self.a3["text"] = "Vector propriu matrice random"
        self.a3.configure(bg="light coral")
        self.a3.grid(row=1, column=0, sticky='nesw')

        self.a4 = Button(self, command = lambda : custom_print(valori_proprii[1]))
        self.a4["text"] = "Valoare proprie matrice 500"
        self.a4.configure(bg="salmon1")
        self.a4.grid(row=2, column=0, sticky='nesw')

        self.a5 = Button(self, command = lambda : custom_print(vector_asociat[1]))
        self.a5["text"] = "Vector propriu matrice 500"
        self.a5.configure(bg="light coral")
        self.a5.grid(row=3, column=0, sticky='nesw')

        self.a6 = Button(self, command = lambda : custom_print(valori_proprii[2]))
        self.a6["text"] = "Valoare proprie matrice 1000"
        self.a6.configure(bg="salmon1")
        self.a6.grid(row=4, column=0, sticky='nesw')

        self.a7 = Button(self, command = lambda : custom_print(vector_asociat[2]))
        self.a7["text"] = "Vector propriu matrice 1000"
        self.a7.configure(bg="light coral")
        self.a7.grid(row=5, column=0, sticky='nesw')

        self.a8 = Button(self, command=lambda: custom_print(valori_proprii[3]))
        self.a8["text"] = "Valoare proprie matrice 1500"
        self.a8.configure(bg="salmon1")
        self.a8.grid(row=6, column=0, sticky='nesw')

        self.a9 = Button(self, command=lambda: custom_print(vector_asociat[3]))
        self.a9["text"] = "Vector propriu matrice 1500"
        self.a9.configure(bg="light coral")
        self.a9.grid(row=7, column=0, sticky='nesw')

        self.a10 = Button(self, command = lambda : custom_print(valori_proprii[4]))
        self.a10["text"] = "Valoare proprie matrice 2019"
        self.a10.configure(bg="salmon1")
        self.a10.grid(row=8, column=0, sticky='nesw')

        self.a11 = Button(self, command = lambda : custom_print(vector_asociat[4]))
        self.a11["text"] = "Vector propriu matrice 2019"
        self.a11.configure(bg="light coral")
        self.a11.grid(row=9, column=0, sticky='nesw')

        self.a12 = Button(self, command = lambda : custom_print(rez_svd[0]))
        self.a12["text"] = "Rangul matricei A"
        self.a12.configure(bg="salmon1")
        self.a12.grid(row=10, column=0,sticky='nesw')

        self.a13 = Button(self, command = lambda : custom_print(rez_svd[3]))
        self.a13["text"] = "Valori singulare matrice A"
        self.a13.configure(bg="light coral")
        self.a13.grid(row=11, column=0,sticky='nesw')

        self.a14 = Button(self, command = lambda : custom_print(rez_svd[1]))
        self.a14["text"] = "Numar conditionare matrice A"
        self.a14.configure(bg="salmon1")
        self.a14.grid(row=12, column=0,sticky='nesw')

        self.a15 = Button(self, command = lambda : custom_print(rez_svd[5]))
        self.a15["text"] = "Pseudo inversa"
        self.a15.configure(bg="light coral")
        self.a15.grid(row=13, column=0,sticky='nesw')

        self.a16 = Button(self, command = lambda : custom_print(rez_svd[4]))
        self.a16["text"] = "x la I"
        self.a16.configure(bg="salmon1")
        self.a16.grid(row=14, column=0,sticky='nesw')

        self.a17 = Button(self, command = lambda : custom_print(rez_svd[6]))
        self.a17["text"] = "Solutia sistemului Ax = b"
        self.a17.configure(bg="light coral")
        self.a17.grid(row=15, column=0,sticky='nesw')

        self.a18 = Button(self, command = lambda : custom_print(rez_svd[7]))
        self.a18["text"] = "Norma"
        self.a18.configure(bg="salmon1")
        self.a17.grid(row=16, column=0,sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=16, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()


solver()
root = Tk()
app = Application(master=root)
app.mainloop()

# metoda_puterii(generare_matrice_fisier("m_rar_sim_2019_1500.txt"), 1500)
# metoda_puterii([[(9,0), (7,1)], [(7,0), (9,1)]], 2)
# write_result_to_file("m_rar_sim_2019_2019.txt", "rez_sim_2019.txt", 2019)


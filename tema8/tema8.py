from tkinter import *
from tkinter import scrolledtext, Text

h= 10**(-5)

def horner(coef,x):
    if len(coef)<1:
        return 0
    b=coef[0]
    for i in range(1, len(coef)):
        b=b*x+coef[i]
    return b

def g1(func,x):
    return (3*horner(func,x)-4*horner(func,x-h)+horner(func,x-2*h))/(2*h)

def g2(func,x):
    return (8*horner(func,x+h)-horner(func,x+2*h)-8*horner(func,x-h)+horner(func,x-2*h))/(12*h)


def computeMinimum1(func,x):
    k=0
    epsilon=10**(-9)
    delta=0
    while True:
        g=g1(func,x)
        if abs(g)<=epsilon:
            return x,k
        delta=g/((g1(func,x+g)-g)/g)
        x=x-delta
        k=k+1
        if abs(delta)<epsilon or k>1000 or abs(delta)>10**8:
            break
    if abs(delta)<epsilon:
        return x,k
    return "divergenta"

def computeMinimum2(func,x):
    k=0
    epsilon=10**(-9)
    delta=0
    while True:
        g=g2(func,x)
        if abs(g)<=epsilon:
            return x,k
        delta=g/((g2(func,x+g)-g)/g)
        x=x-delta
        k=k+1
        if abs(delta)<epsilon or k>1000 or abs(delta)>10**8:
            break
    if abs(delta)<epsilon:
        return x,k
    return "divergenta"

# print(computeMinimum1([1,-4,3]))
# print(computeMinimum2([1,-4,3]))

def verifyIfMin(func,x):
    dev2=(16*horner(func,x+h)-horner(func,x+2*h)-30*horner(func,x)+16*horner(func,x-h)-horner(func,x-2*h))/(12*h*h)
    return dev2, dev2>0


def custom_print(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(to_print)))
    return
    
f=[1,-6,13,-12,4]
x0=3.0
min1=computeMinimum1(f,x0)
min2=computeMinimum2(f,x0)
verif1 = verifyIfMin(f,min1[0])
verif2 = verifyIfMin(f,min2[0])

class Application(Frame):
    def createWidgets(self):
        self.a2 = Button(self , command = lambda : custom_print(min1))
        self.a2["text"] = "Minim 1"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=0, column=0, sticky='nesw')

        self.a4 = Button(self , command = lambda : custom_print(min2))
        self.a4["text"] = "Minim 2"
        self.a4.configure(bg="salmon1")
        self.a4.grid(row=1, column=0, sticky='nesw')

        self.a3 = Button(self , command = lambda : custom_print(verif1))
        self.a3["text"] = "Verificare minim 1"
        self.a3.configure(bg="light coral")
        self.a3.grid(row=2, column=0, sticky='nesw')

        self.a3 = Button(self , command = lambda : custom_print(verif2))
        self.a3["text"] = "Verificare minim 2"
        self.a3.configure(bg="light coral")
        self.a3.grid(row=3, column=0, sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=4, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
from tkinter import *
from tkinter import scrolledtext
epsilon = 0.000001

def readFromFile(fileName):
    f = open(fileName)
    fileLines=[]
    for line in f:
        if len(line.strip())>0:
            fileLines.append(line.strip())
    lines=int(fileLines[0])
    matrix=[[] for i in range(0,lines)]
    i=1
    l=fileLines[i].split(", ")
    while(len(l)>1):
        value=(float(l[0]),int(l[1]),int(l[2]))
        if exists(matrix,value[1],value[2])==False:
            matrix[value[1]].append((float(l[0]),int(l[2])))
        else:
            addValueWhenIdenticalIndex(matrix,value[1],value[2],value[0])
        i+=1
        l=fileLines[i].split(", ")
    b=[]
    for j in range(i, len(fileLines)):
        b.append(float(fileLines[j]))
    return matrix,b,lines


def exists(m,i,j):
    for e in m[i]:
        if e[1]==j:
            return True
    return False


def addValueWhenIdenticalIndex(m,i,j,value):
    for index in range(0,len(m[i])):
        if m[i][index][1]==j:
             m[i][index]=(m[i][index][0]+value,j)


def positionOfIndices(m,i,j):
    for index in range(0,len(m[i])):
        if m[i][index][1]==j:
             return index
    return None


def addMatrix(m1,m2,n1,n2):
    if n1!=n2:
        return None
    mPlus=[[] for i in range(0,n1)]
    for i in range(0,n1):
        for j in range(0,n1):
            m1Pos=positionOfIndices(m1,i,j)
            m2Pos=positionOfIndices(m2,i,j)
            if m1Pos!=None and m2Pos!=None:
                mPlus[i].append((m1[i][m1Pos][0]+m2[i][m2Pos][0],j))
            if m1Pos==None and m2Pos!=None:
                mPlus[i].append((m2[i][m2Pos][0],j))
            if m1Pos!=None and m2Pos==None:
                mPlus[i].append((m1[i][m1Pos][0],j))
    return mPlus

def multiplyMatrix(m1,m2,n):
    mMult=[]
    if n!=1:
        mMult=[[] for i in range(0,len(m1))]
        for i in range(len(m1)):
            for j in range(0,len(m2)):
                result=0
                for e in m1[i]:
                    k=e[1]
                    m1Pos=positionOfIndices(m1,i,k)
                    m2Pos=positionOfIndices(m2,k,j)
                    if m1Pos!=None and m2Pos!=None:
                        result += m1[i][m1Pos][0] * m2[k][m2Pos][0]
                if result!=0:
                    mMult[i].append((result,j))
    else:
        mMult=[0]*len(m1)
        for i in range(len(m1)):
            for k in range(len(m2)):
                m1Pos=positionOfIndices(m1,i,k)
                if m1Pos!=None:
                    mMult[i] += m1[i][m1Pos][0] * m2[k]
    return mMult


def areEqualVectors(x,y):
    if len(x)!=len(y):
        return False
    for i in range(0,len(x)):
        if abs(x[i]-y[i])>epsilon:
            return False
    return True


def checkMatrixConstraint(m,n,maxValues):
    for i in range(0,n):
        if len(m[i])>maxValues:
            return False
    return True


def areEqual(m1,m2,n1,n2):
    if n1!=n2:
        return False
    for i in range(0,n1):
        for j in range(0,n1):
            m1Pos=positionOfIndices(m1,i,j)
            m2Pos=positionOfIndices(m2,i,j)
            if m1Pos!=None and m2Pos!=None:
                if abs(m1[i][m1Pos][0]-m2[i][m2Pos][0])>epsilon:
                    return False
            if m1Pos!=None and m2Pos==None:
                return False
            if m1Pos==None and m2Pos!=None:
                return False
    return True

def create_multiply_vector():
    x=[i for i in range(2019,0,-1)]
    return x


def solve():
    global A, b1, n1, B, b2, n2, AddMatrixFile, OurAddMatrix, bplus, nplus, constraints12, constraints24
    global MultiplyMatrixFile, OurMultiplyMatrix, multVector, equalMultVector, areEqualSums, areEqProductMatrix
    global vecA, vecM, ourA, ourM, A1, B1, ourV
    A,b1, n1 = readFromFile("a.txt")
    B,b2,n2=readFromFile("b.txt")
    A1=[A[j] for j in range(0,10)]
    B1=[B[j] for j in range(0,10)]
    AddMatrixFile,bplus,nplus=readFromFile("aplusb.txt")
    vecA=[AddMatrixFile[j] for j in range(0,10)]
    MultiplyMatrixFile, bmult, nmult = readFromFile("aorib.txt")
    vecM = [MultiplyMatrixFile[j] for j in range(0, 10)]
    OurAddMatrix = addMatrix(A,B,n1,n2)
    OurMultiplyMatrix = multiplyMatrix(A, B, n1)
    ourA = [OurAddMatrix[j] for j in range(0, 10)]
    ourM = [OurMultiplyMatrix[j] for j in range(0, 10)]
    multVector = multiplyMatrix(B, create_multiply_vector(), 1)
    ourv = [multVector[j] for j in range(0, 10)]
    equalMultVector = areEqualVectors(b2,multVector)
    areEqualSums = areEqual(AddMatrixFile,OurAddMatrix,nplus,nplus)
    areEqProductMatrix = areEqual(AddMatrixFile,OurAddMatrix,nplus,nplus)
    constraint12 = (checkMatrixConstraint(A,n1,12)) and (checkMatrixConstraint(B,n1,12))
    constraints12 = "A and B have maximum 12 elements !=0 on each row/column: " + str(constraint12)
    constraints24 = "A and B sum have maximum 24 elements !=0 on each row/column: " + str(checkMatrixConstraint(OurAddMatrix,nplus,24))
    return

def customPrint(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(to_print))
    return

class Application(Frame):
    def createWidgets(self):
        self.a1 = Button(self, command = lambda : customPrint(A1))
        self.a1["text"] = "Rare Matrix A"
        self.a1.configure(bg="salmon1")
        self.a1.grid(row=0, column=0, sticky='nesw')

        self.a2 = Button(self , command = lambda : customPrint(B1))
        self.a2["text"] = "Rare Matrix B"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=1, column=0, sticky='nesw')

        self.a3 = Button(self, command = lambda : customPrint(vecA))
        self.a3["text"] = "Result of adding A and B"
        self.a3.configure(bg="salmon1")
        self.a3.grid(row=2, column=0, sticky='nesw')

        self.a4 = Button(self, command = lambda : customPrint(vecM))
        self.a4["text"] = "Rare matrix of sum from File"
        self.a4.configure(bg="salmon1")
        self.a4.grid(row=3, column=0, sticky='nesw')

        self.a5 = Button(self, command = lambda : customPrint(ourM))
        self.a5["text"] = "Result of multiplying A and B"
        self.a5.configure(bg="salmon1")
        self.a5.grid(row=4, column=0, sticky='nesw')

        self.a6 = Button(self, command = lambda : customPrint(MultiplyMatrixFile))
        self.a6["text"] = "Rare matrix of product from File"
        self.a6.configure(bg="salmon1")
        self.a6.grid(row=5, column=0, sticky='nesw')

        self.a7 = Button(self, command = lambda : customPrint(ourV))
        self.a7["text"] = "Result of multiplying matrix A with vector x"
        self.a7.configure(bg="salmon1")
        self.a7.grid(row=6, column=0, sticky='nesw')

        self.a8 = Button(self, command=lambda: customPrint(areEqualSums))
        self.a8["text"] = "Are the sums equal?"
        self.a8.configure(bg="light coral")
        self.a8.grid(row=7, column=0, sticky='nesw')

        self.a9 = Button(self, command=lambda: customPrint(areEqProductMatrix))
        self.a9["text"] = "Are the products equal?"
        self.a9.configure(bg="light coral")
        self.a9.grid(row=8, column=0, sticky='nesw')

        self.a10 = Button(self, command = lambda : customPrint(equalMultVector))
        self.a10["text"] = "Is matrix-vector multiplication's result the vector b?"
        self.a10.configure(bg="light coral")
        self.a10.grid(row=9, column=0, sticky='nesw')

        self.a11 = Button(self, command = lambda : customPrint(constraints12))
        self.a11["text"] = "Are the constraints satisfied for A and B?"
        self.a11.configure(bg="light coral")
        self.a11.grid(row=10, column=0, sticky='nesw')

        self.a12 = Button(self, command = lambda : customPrint(constraints24))
        self.a12["text"] = "Are the constraints satisfied for A and B sum?"
        self.a12.configure(bg="light coral")
        self.a12.grid(row=11, column=0,sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=12, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()


solve()
root = Tk()
app = Application(master=root)
app.mainloop()

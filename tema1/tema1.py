import math
import random
import time
from tkinter import *
from tkinter import scrolledtext


def fact(n):
    rez = 1
    while n != 1:
        rez = rez * n
        n -= 1
    return rez


c1=1/fact(3)
c2=1/fact(5)
c3=1/fact(7)
c4=1/fact(9)
c5=1/fact(11)
c6=1/fact(13)


def p1(x):
    y=x*x
    return x*(1+y*(-c1+c2*y))


def p2(x):
    y=x*x
    return x*(1+y*(-c1+y*(c2-c3*y)))


def p3(x):
    y=x*x
    return x*(1+y*(-c1+y*(c2+y*(-c3+c4*y))))


def p4(x):
    y = x * x
    return x*(1+y*(-0.166+y*(0.00833+y*(-c3+c4*y))))


def p5(x):
    y = x * x
    return x * (1 + y * (-c1 + y * (c2+ y * (-c3 + y*(c4-c5*y)))))


def p6(x):
    y = x * x
    return x * (1 + y * (-c1 + y * (c2+ y * (-c3 + y*(c4+y*(-c5+c6*y))))))


def sinList(sin,x):
    list=[]
    list.append(abs(sin-p1(x)))
    list.append(abs(sin-p2(x)))
    list.append(abs(sin-p3(x)))
    list.append(abs(sin-p4(x)))
    list.append(abs(sin-p5(x)))
    list.append(abs(sin-p6(x)))
    #print(x)
    #print(sin,p1(x), p2(x),p3(x),p4(x),p5(x),p6(x))
    #print(list)
    return list


def max3(list):
    newList=[]
    newList.append(list.index(min(list)))
    list[newList[0]]=2
    newList.append(list.index(min(list)))
    list[newList[1]]=2
    newList.append(list.index(min(list)))
    return newList


def problema3():
    app.panel.delete(1.0, END)
    pol={1:0,2:0,3:0,4:0,5:0,6:0}
    for i in range(0,10000):
        x=random.uniform(-math.pi/2,math.pi/2)
        sin=math.sin(x)
        max3Pol=max3(sinList(sin,x))
        #print(max3Pol)
        pol[max3Pol[0]+1]+=1
        pol[max3Pol[1]+1]+=0.5
        pol[max3Pol[2]+1]+=0.25
    app.panel.insert(END, repr(sorted(pol.keys(),key= lambda x:pol[x], reverse=True)))
    return sorted(pol.keys(),key= lambda x:pol[x], reverse=True)


def executionTime(valueSet, func):
    t=time.time()
    for i in valueSet:
        func(i)
    return time.time()-t


def problema3Time():
    app.panel.delete(1.0, END)
    pol={1:0,2:0,3:0,4:0,5:0,6:0}
    valueSet=set()
    for i in range(0,10000):
        valueSet.add(random.uniform(-math.pi/2,math.pi/2))
    pol[1]=executionTime(valueSet,p1)
    pol[2]=executionTime(valueSet,p2)
    pol[3]=executionTime(valueSet,p3)
    pol[4]=executionTime(valueSet,p4)
    pol[5]=executionTime(valueSet,p5)
    pol[6]=executionTime(valueSet,p6)
    app.panel.insert(END, repr(sorted(pol.items(),key= lambda x:x[1])))
    return sorted(pol.items(),key= lambda x:x[1])

# print(problema3())
# print(problema3Time())


def problema1():
    app.panel.delete(1.0, END)
    m = 0
    while (float(1) + float(10 ** (-m))) != float(1):
        m += 1
    m += -1
    precizie = 10 ** (-m)
    app.panel.insert(END, repr(precizie))
    return precizie


def problema2():
    app.panel.delete(1.0, END)
    x = 1.0
    y = problema1()
    z = problema1()
    app.panel.delete(1.0, END)
    rez = True
    rez1 = float((x+y)+z)
    rez2 = float(x+(y+z))
    if rez1 == rez2:
        rez = False
    app.panel.insert(END, repr(rez))
    return rez


# print(problema2())


def problema2inmultire():
    app.panel.delete(1.0, END)
    nr1 = random.uniform(0.0, 10.0)
    nr2 = random.uniform(0.0, 10.0)
    nr3 = random.uniform(0.0, 10.0)
    while (nr1*nr2)*nr3 == nr1*(nr2*nr3):
        nr1 = random.uniform(0.0, 10.0)
        nr2 = random.uniform(0.0, 10.0)
        nr3 = random.uniform(0.0, 10.0)
    app.panel.insert(END, repr((nr1,nr2,nr3)))
    return (nr1, nr2, nr3)

class Application(Frame):
    def createWidgets(self):
        self.ex1 = Button(self, command = lambda : problema1())
        self.ex1["text"] = "Ex1"
        self.ex1.pack({"side": "left"})

        self.ex2 = Button(self , command = lambda : problema2())
        self.ex2["text"] = "Ex2"
        self.ex2.pack({"side": "left"})

        self.ex2b = Button(self, command = lambda : problema2inmultire())
        self.ex2b["text"] = "Ex2b"
        self.ex2b.pack({"side": "left"})

        self.ex3 = Button(self, command = lambda : problema3())
        self.ex3["text"] = "Ex3"
        self.ex3.pack({"side": "left"})

        self.ex3b = Button(self, command = lambda : problema3Time())
        self.ex3b["text"] = "Ex3b"
        self.ex3b.pack({"side": "left"})

        self.panel = scrolledtext.ScrolledText(self, height=20, width=150)
        self.panel.pack(side=BOTTOM)

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()
import numpy
from numpy import *

global epsilon
copyA=[]

def isMatrixDecomposable(A):
    for p in range (1,len(A)+1):
        if numpy.linalg.det([y[:p] for y in A[:p]])==0:
            return False
    return True


def computeLU(A):
    if(isMatrixDecomposable(A)):
        for p in range(0,len(A)):
            if A[p][p] == 0:
                return None
            for i in range(0,p):
                if(abs( A[i][i])>epsilon):
                    A[i][p] = (A[i][p] - sum([A[i][k] * A[k][p] for k in range(0, i)])) / A[i][i]
                else:
                    print("Cannot divide the numbers")
            for i in range(0, p+1):
                A[p][i] = A[p][i] - sum([A[p][k] * A[k][i] for k in range(0, i)])
        return A
    else:
        print("Matrix is not decomposable")
    return None


def computeLUDeterminant(A):
    global copyA
    copyA=A.copy()
    computeLU(A)
    #L
    for i in range(0,len(A)):
        for j in range(i+1,len(A)):
            A[i][j]=0
    det=numpy.linalg.det(A)
    A= copyA.copy()
    #U
    for i in range(0,len(A)):
        A[i][i]=1
        for j in range(0,i):
            A[i][j]=0
    det=det*numpy.linalg.det(A)
    return det


def solveSystem(A,b):
    global copyA
    copyA = A.copy()
    y=[]
    computeLU(A)
    for i in range(0,len(b)):
        if abs(A[i][i])>epsilon:
            y.append((b[i]-sum([A[i][j]*y[j] for j in range(0,i)]))/A[i][i])
            continue
        else:
            print("Cannot divide numbers")
    x=[0 for i in range(0, len(b))]
    for i in range(len(b)-1, -1, -1):
        x[i]=y[i]-sum([A[i][j]*x[j] for j in range(i+1,len(b))])
    return x


def computeNormWithLU(A,b):
    global copyA
    copyA = A
    x=solveSystem(A,b)
    val = subtract(copyA*x,b)
    norm = linalg.norm(val,2)
    return norm

def computeNorms(A,b):
    global copyA
    copyA = A.copy()
    xlu = solveSystem(A,b)
    A = copyA.copy()
    xlib = solveSystem(A,b)
    A = copyA.copy()
    val1 = subtract(xlu, xlib)
    norm1 = linalg.norm(val1)
    inverse = solveInverseNumpy(A)
    val2 = subtract(xlu, inverse*b)
    norm2 = linalg.norm(val2)
    print("x lu:")
    print(xlu)
    print("x lib:")
    print(xlib)
    return norm1, norm2


def solveSystemNumpy(A,b):
    return linalg.solve(A,b)


def solveInverseNumpy(A):
    return linalg.inv(A)


def buildA(n):
    return random.random((n, n))


def buildB(n):
    return random.random(n)


def runFunctions():
    global epsilon
    try:
        t = int(input('Enter epsilon power:'))
        epsilon = 10 ** (-t)
        n = int(input('Data dimension:'))
        A = buildA(n)
        b = buildB(n)
        print("Compute LU")
        print(computeLU(A))
        print("Compute LU determinant")
        print(computeLUDeterminant(A))
        print("Solve system")
        print(solveSystem(A,b))
        print("Compute norms")
        print(computeNorms(A,b))
        print("Compute LU norm")
        print(computeNormWithLU(A,b))
        return
    except ValueError:
        print("Not a number")


runFunctions()
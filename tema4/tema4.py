import numpy as numpy
from tkinter import *
from tkinter import scrolledtext

def read_from_file(file_name):
    f = open(file_name)
    file_lines = []
    for line in f:
        if len(line.strip()) > 0:
            file_lines.append(line.strip())
    lines = int(file_lines[0])
    matrix = [[] for i in range(0, lines)]
    i = 1
    l = file_lines[i].split(", ")
    while len(l)>1:
        value=(float(l[0]), int(l[1]), int(l[2])+1)
        if not exists(matrix, value[1], value[2]):
            matrix[value[1]].append((float(l[0]), int(l[2])+1))
        else:
            add_value_when_identical_index(matrix, value[1], value[2], value[0])
        i += 1
        l = file_lines[i].split(", ")
    b = []
    for j in range(i, len(file_lines)):
        b.append(float(file_lines[j]))
    return matrix, b, lines


def exists(m, i, j):
    for e in m[i]:
        if e[1] == j:
            return True
    return False


def add_value_when_identical_index(m, i, j, value):
    for index in range(0, len(m[i])):
        if m[i][index][1] == j:
            m[i][index] = (m[i][index][0]+value, j)


def maximum_elem_nenule_linie(matrix):
    maxr = 0
    for line in matrix:
        max_local = 0
        for i in range(0, len(line)):
            max_local += 1
        if max_local > maxr:
            maxr = max_local
    return maxr


def nr_el_nenule_linie(matrix, i):
    return len(matrix[i])


def nr_elem_nenule_matrice(matrix, n):
    nr_elem_nenule = 0
    for i in range(0, len(matrix)):
        nr_elem_nenule += len(matrix[i])
    nr_elem_nenule = n*n - nr_elem_nenule
    return nr_elem_nenule


def construct_valori_matrix(matrix, n):
    valori, ind_col = memorare_comprimata_pe_linii(matrix)
    nr_col = maximum_elem_nenule_linie(matrix)
    nr_linii = n
    value_matrix = [[0]*nr_col for i in range(0, nr_linii)]
    indice_matrix = [[0]*nr_col for i in range(0, nr_linii)]
    for i in range(0, nr_linii):
        nr_eln_linie = nr_el_nenule_linie(matrix, i)
        for j in range(0, nr_col):
            if j < nr_eln_linie:
                value_matrix[i][j] = valori[00]
                indice_matrix[i][j] = ind_col[0]
                valori.pop(00)
                ind_col.pop(0)
    return value_matrix, indice_matrix


def memorare_comprimata_pe_linii(matrix):
    valori = []
    ind_col = []
    for i in range(0, len(matrix)):
        for el in matrix[i]:
            valori.append(el[0])
            ind_col.append(el[1])
    return (valori, ind_col)


def check_eln_diag(matrix):
    for i in range(0, len(matrix)):
        for j in range(0, len(matrix[i])):
            if i == matrix[i][j][1]:
                if abs(matrix[i][j][0]) < 0.0000001:
                    return False
    return True


def get_value_at_index(valori, ind_col, nr_col, i, j):
    col = 1
    while col < nr_col+1 and col != 0:
        if ind_col[i-1][col-1] == j:
            to_ret = valori[i-1][col-1]
            return to_ret
        col += 1
    return 0


def inmultire(valori, ind_col, vec):
    rez = [0.0]*len(vec)
    for rand in range(1, len(vec)+1):
        for coloana in range(1, len(vec)+1):
            rez[rand-1] += get_value_at_index(valori, ind_col, len(ind_col[0]), rand, coloana)*vec[rand-1]
    return rez


def calcularea_normei(inmult, b, iter):
    multnp = numpy.array(inmult)
    bnp = numpy.array(b)
    s = multnp - bnp
    rez = 0
    for i in range(0, len(s)):
        rez += abs(s[i])**iter
    rez = rez ** (1/iter)
    return rez


def start(fisier):
    M ,b, n = read_from_file(fisier)
    return (M, b, n)


def ex1(M):
    for el in M:
        print(check_eln_diag(el))
    return


def ex2(M, b, n):
    max_iterations = 3
    epsilon = 1/10**6
    valori, ind_col = construct_valori_matrix(M, n)
    # current_solution = [0.0] * n
    # previous_solution = [0.0] * n
    current_solution=[1.0, 2.0, 3.0,4.0, 5.0]
    previous_solution= [1.0, 2.0, 3.0,4.0, 5.0]
    w = 1.0
    iter = 1
    while True:
        error = []
        for i in range(1, n+1):
            previous_solution = current_solution[:]
            sum_of_lower_elements = 0.0
            for j in range(1, i):
                sum_of_lower_elements += get_value_at_index(valori, ind_col, len(ind_col[0]), i, j)\
                                         * current_solution[j-1]
            sum_of_upper_elements = 0.0
            for j in range(i+1, n+1):
                sum_of_upper_elements += get_value_at_index(valori, ind_col, len(ind_col[0]), i, j)\
                                         * previous_solution[j-1]
            current_solution[i-1] = (1-w)*previous_solution[i-1]
            to_mult = b[i-1] - sum_of_upper_elements - sum_of_lower_elements
            current_solution[i-1] += (w/get_value_at_index(valori, ind_col, len(ind_col[0]), i, i))*to_mult
        er = 1
        for indice in range(0, n):
            error.append(abs(current_solution[indice]-previous_solution[indice]))
            if error[indice] > epsilon:
                er = 0
                break
        if er == 1:
            return iter, current_solution
        iter += 1
        if iter > max_iterations:
            return iter, current_solution


def solve(fisier):
    global iters, sols, checks
    M, b, n = start(fisier)
    valori, ind_col = construct_valori_matrix(M,n)
    iters, sols = ex2(M, b, n)
    rez = inmultire(valori, ind_col, sols)
    checks = calcularea_normei(rez, b, iters-1)
    print("Iter:" + str(iter))
    print("Sol" + str(sols))
    print("Check" + str(checks))
    return sols, iters, checks


def customPrint(to_print):
    app.panel.delete(1.0, END)
    app.panel.insert(END, repr(str(to_print)))
    return


class Application(Frame):
    def createWidgets(self):
        self.a1 = Button(self, command = lambda : customPrint(sols))
        self.a1["text"] = "Solution Matrix"
        self.a1.configure(bg="salmon1")
        self.a1.grid(row=0, column=0, sticky='nesw')

        self.a2 = Button(self , command = lambda : customPrint(checks))
        self.a2["text"] = "Check Result"
        self.a2.configure(bg="salmon1")
        self.a2.grid(row=1, column=0, sticky='nesw')

        self.a3 = Button(self , command = lambda : customPrint(iters-1))
        self.a3["text"] = "Iterations"
        self.a3.configure(bg="salmon1")
        self.a3.grid(row=2, column=0, sticky='nesw')

        self.panel = scrolledtext.ScrolledText(self, height=20, width=100, bg="azure")
        self.panel.grid(row=0, column=1, rowspan=3, sticky='nesw')

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()


solve("matrice_rara_ex.txt")
root = Tk()
app = Application(master=root)
app.mainloop()